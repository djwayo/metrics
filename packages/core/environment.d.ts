declare global {
    namespace NodeJS {
        interface ProcessEnv {
            API_KEY: string;
            EMAIL: string;
            NAME: string;
        }
    }
}

export { }