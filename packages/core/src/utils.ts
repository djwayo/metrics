export const sumValues = (obj:any) => Object.values(obj).reduce((a:any, b) => a + b);

export function daysInMonth (month: number, year: number) {
    return new Date(year, month, 0).getDate();
}

export function getWeekNumber(date: Date) {
    let onejan = new Date(date.getFullYear(), 0, 1);
    let week = Math.ceil((((date.getTime() - onejan.getTime()) / 86400000) + onejan.getDay() + 1) / 7);
    return week;
}

export function groupArrayOfObjects(list: any, key: any) {
    return list.reduce((rv: any, x: any,) => {
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

export function groupBay(list:any, keyGetter:any) {
    const map = new Map();
    list.forEach((item:any) => {
         const key = keyGetter(item);
         const collection = map.get(key);
         if (!collection) {
             map.set(key, [item]);
         } else {
             collection.push(item);
         }
    });
    return map;
}