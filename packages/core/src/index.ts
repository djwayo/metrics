import { Api } from "@metrics/api";
import { AnalyticsByYear, BaseData, PostByWeekNumber, PostParsed, ResponsePost } from "./types";
import { daysInMonth, getWeekNumber, groupArrayOfObjects, sumValues } from "./utils";
require("dotenv").config();

export class Metrics {

  api: Api
  allPosts: PostParsed[] = [];
  _maxPostPages: number = 10;

  constructor() {
    this.api = new Api(process.env.API_KEY, process.env.EMAIL, process.env.NAME);
  }

  /**
   * Add the needed parameters per post to get the metrics
   * @param post 
   * @returns PostParsed
   */
  getDataPerPost(post: ResponsePost): PostParsed {
    const value = post.message.length;
    const date = new Date(post.created_time)
    const month = date.getMonth() + 1;
    const year = date.getFullYear();
    const weekNumber = getWeekNumber(date);
    const id = post.id;
    const message = post.message;
    return {
      value,
      month,
      year,
      weekNumber,
      id,
      message
    }
  }

  /**
   * Calculate and save in memory the necessary data to get the analytics
   * @returns Promise<BaseData>
   */
  async calculateBaseData(): Promise<BaseData> {
    let baseData: BaseData = {
      posts: [],
      postsCount: 0,
      totalCharacters: 0,
      users: []
    };
    for (let index = 1; index <= this._maxPostPages; index++) {
      const posts = await this.api.posts(index);
      if (!posts?.data.posts) return baseData;
      for await (const iterator of posts?.data.posts) {
        const element: ResponsePost = iterator;
        const { year, month, value, weekNumber, id, message } = this.getDataPerPost(element)

        this.allPosts.push({ year, month, value, weekNumber, id, message })

        if (!baseData[year]) {
          baseData[year] = {}
        }
        if (!baseData[year][month]) {
          baseData[year][month] = { totalCharacters: 0, users: [], postsCount: 0, posts: [] }
        }

        baseData[year][month].totalCharacters = baseData[year][month].totalCharacters + value

        const user = element.from_id
        baseData[year][month].users.push(user)

        baseData[year][month].posts.push({
          id,
          message,
          weekNumber,
          month,
          year
        })

        baseData[year][month].postsCount += 1

      }
    }
    return baseData;
  }


  /**
   * Calculate the average of the users post per month
   * based on array that contains multiple users based on all month posts
   * @param users 
   * @returns number
   */
  getAverageUserPostPerMont(users: string[]): number {
    const map: any = users.reduce((a: string, c: string) => (a[c] = a[c] || 0, a[c]++, a), {} as "");
    const output = Object.keys(map).map((value: string) => ({
      name: value,
      count: map[value]
    }));
    return output.reduce((a, { count }) => a + count, 0) / output.length;
  }


  /**
   * Get largest post based on posts from the same month
   * @param posts 
   * @returns 
   */
  getLongestCharPostPerMonth(posts: PostParsed[]) {
    let chars: [{ id: string, value: number }] | any[] = [];
    posts.forEach(element => {
      chars.push({
        id: element.id,
        value: element.message.length
      });
    });
    return chars.reduce((prev, current) => (prev.value > current.value) ? prev : current)
  }

  /**
   * Get all posts grouped by same week number, based on all posts fetched
   * @param posts 
   * @returns number
   */
  getPostsByWeekNumber(posts: PostParsed[]) {
    return posts.reduce((r, a) => { r[a.weekNumber] = [...r[a.weekNumber] || [], a]; return r; }, {});
  }

  /**
   *  Execute the logic to get the analytics by month and splitd by weeks
   * @returns analyticsByMonth: AnalyticsByYear; getPostsByWeekNumber: PostByWeekNumber;
   */
  async calculateAnalytics(): Promise<{
    analyticsByMonth: AnalyticsByYear;
    getPostsByWeekNumber: PostByWeekNumber;
  }> {
    await this.api.authenticate();
    const baseData = await this.calculateBaseData();
    let analyticsByMonth: AnalyticsByYear = {};
    for (const mainKey in baseData) {
      const element = baseData[mainKey];
      analyticsByMonth[mainKey] = {};
      Object.keys(element).forEach(key => {
        const monthData = element[key]
        const monthDays = daysInMonth(Number(key), Number(mainKey));
        const averageChars = monthData.totalCharacters / monthDays;
        const averageUsersPostPerMonth = this.getAverageUserPostPerMont(monthData.users);
        const getLongestCharPostPerMonth = this.getLongestCharPostPerMonth(monthData.posts)
        element[key] = {
          averageChars,
          averageUsersPostPerMonth,
          getLongestCharPostPerMonth,
        }
        analyticsByMonth[mainKey][key] = {
          averageChars,
          averageUsersPostPerMonth,
          getLongestCharPostPerMonth,
        }
      });
    }
    const getPostsByWeekNumber = this.getPostsByWeekNumber(this.allPosts)
    return { analyticsByMonth, getPostsByWeekNumber };
  }
}