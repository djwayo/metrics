export interface ResponsePost {
    id: string;
    from_name: string;
    from_id: string;
    message: string;
    type: string;
    created_time: Date;
}

export interface PostByWeekNumber {
    [key: string]: PostParsed[];
}

export interface PostParsed {
    year: number;
    month: number;
    value: number;
    weekNumber: number;
    id: string;
    message: string;
}

export interface BaseData {
    totalCharacters: number;
    users: string[];
    postsCount: number;
    posts: PostParsed[];
}

export interface AnalyticsByYear {
    [key: string]: AnalyticsByMonth;
}

export interface AnalyticsByMonth {
    [key: string]: AnalyticsByMonthDetail;
}

export interface AnalyticsByMonthDetail {
    averageChars: number;
    averageUsersPostPerMonth: number;
    getLongestCharPostPerMonth: GetLongestCharPostPerMonth;
}

export interface GetLongestCharPostPerMonth {
    id: string;
    value: number;
}
