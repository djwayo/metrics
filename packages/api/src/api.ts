import got from 'got';
import { API_URL, POST_ENDPOINT, REGISTER_ENDPOINT } from './constants';
import { RegisterResponse } from './models/register-response';
import { PostResponse } from './models/posts-response';

export class Api {
    client_id: string;
    email: string;
    name: string;

    private _token: string | undefined;
    private _retryCount: number;
    private _maxRetryCount: number;

    constructor(client_id: string, email: string, name: string) {
        if(client_id === undefined || email === undefined ) {
            throw new Error('client_id or  email is undefined');
          }
        this.client_id = client_id;
        this.email = email;
        this.name = name;
        this._retryCount = 0;
        this._maxRetryCount = 5;
    }

    async authenticate() {
        try {
            const { statusCode, body } = await got.post(`${API_URL + REGISTER_ENDPOINT}`, {
                json: {
                    client_id: this.client_id,
                    email: this.email,
                    name: this.name
                },
                responseType: 'json'
            });

            if (statusCode === 200) {
                const typeBody = body as RegisterResponse;
                this._token = typeBody.data.sl_token;
                return 'ok'
            } else {
                throw new Error("Cannot reach the api with the given credentials");
            }

        } catch (error: any) {
            console.log('errr')
            console.log(error.response.statusCode);
            return error;
        }
    }

    async posts(page: number): Promise<PostResponse | undefined> {
        try {
            const { body } = await got.get(`${API_URL + POST_ENDPOINT}?sl_token=${this._token}&page=${page}`, { responseType: 'json' });
            return body as PostResponse;
        } catch (error: any) {
            if (this._retryCount <= this._maxRetryCount) {
                console.log('retyr...')
                await this.authenticate()
                await this.posts(page);
            } else {
                return error;
            }
        }
    }
}