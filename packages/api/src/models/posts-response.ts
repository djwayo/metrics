export interface PostResponse {
    meta: Meta;
    data: Data;
}

export interface Data {
    page:  number;
    posts: Post[];
}

export interface Post {
    id:           string;
    from_name:    string;
    from_id:      string;
    message:      string;
    type:         Type;
    created_time: Date;
}

export enum Type {
    Status = "status",
}

export interface Meta {
    request_id: string;
}
