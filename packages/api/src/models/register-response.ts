export interface RegisterResponse {
    meta: Meta;
    data: Data;
}

export interface Data {
    client_id: string;
    email:     string;
    sl_token:  string;
}

export interface Meta {
    request_id: string;
}
