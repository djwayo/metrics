import { Metrics } from '@metrics/core';
import { writeFile } from "fs/promises";
import millify from "millify";
import path from "path";
import logSymbols from 'log-symbols';
const { Command } = require('commander');
const Table = require('cli-table3');
const program = new Command();

program
  .name('string-util')
  .description('CLI to some JavaScript string utilities')
  .version('0.8.0');

program.command('a')
  .description('Split a string into substrings and display as an array')
  .action(async () => {
    const metrics = new Metrics();
    console.log(logSymbols.info, 'Getting data...');
    const { analyticsByMonth, getPostsByWeekNumber } = await metrics.calculateAnalytics();
    writeFile(path.join(process.cwd(), `/stats.json`), JSON.stringify({ analyticsByMonth, ...getPostsByWeekNumber }));
    const analyticsByMonthTable = new Table({
      head: ['YEAR', 'MONTH', 'AVERAGE CHARS', 'LONGEST POST BY CHARACTERS', 'AVERAGE USER POSTS'],
      colWidths: [6, 7, 20, 45, 23]
    });
    const getPostsByWeekNumberTable = new Table({
      head: ['WEEK', 'NUMBER OF POSTS'],
      colWidths: [8, 20]
    });
    for (const yearKey in analyticsByMonth) {
      const year = analyticsByMonth[yearKey];
      for (const monthKey in year) {
        const month = year[monthKey];
        analyticsByMonthTable.push(
          [yearKey, monthKey, millify(month.averageChars), `id:${month.getLongestCharPostPerMonth.id} - lenght:${millify(month.getLongestCharPostPerMonth.value)}`, millify(month.averageUsersPostPerMonth)]
        );
      }

    }

    for (const weekKey in getPostsByWeekNumber) {
      const item = getPostsByWeekNumber[weekKey];
      getPostsByWeekNumberTable.push(
        [weekKey, item.length]
      );
    }

    console.log(analyticsByMonthTable.toString());
    console.log(getPostsByWeekNumberTable.toString());
    console.log(logSymbols.success, 'Finished successfully!');
  });

program.parse();