# Metrics monorepo

The project is built with npm workspaces as monorepo

Check my GitHub to see other projects https://github.com/DonsWayo

![](./a.png)
![](./b.png)
# installation
```sh
# npm version >= 7
$ npm i
```
# Credentials
create new .env file from the .env.sample with your credentials


# Packages
## api
Makes the api calls to get the posts and used by other package as sdk

```typescript
const api = new Api('<SECRET>','<EMAIL>', '<NAME>')
const posts = await api.authenticate();
const posts = await api.posts(10);
```


## cli
execute the invocation to get the analytics in a table outputs and JSON file called stats on the root path when you called the cli

```sh
cd packages/cli
npm link
mcli a
```

## core
In this package is the main logic to get the analytics

```typescript
const metrics = new Metrics();
const { analyticsByMonth, getPostsByWeekNumber } = await metrics.calculateAnalytics();
```